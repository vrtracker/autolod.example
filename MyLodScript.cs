﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityMeshSimplifier;

public class MyLodScript : MonoBehaviour
{
    // Start is called before the first frame update

    public Mesh SourceMesh;
    public GameObject Skin;

    [Range(0f, 1f)] 
    public float Quality;

    void Start()
    {
        /*var levels = new LODLevel[]
        {
            new LODLevel(0f, 0.5f)
            {
                CombineMeshes = false,
                CombineSubMeshes = false,
                SkinQuality = SkinQuality.Auto,
                ShadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On,
                ReceiveShadows = true,
                SkinnedMotionVectors = true,
                LightProbeUsage = UnityEngine.Rendering.LightProbeUsage.BlendProbes,
                ReflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.BlendProbes,
            }
        };
        LODGenerator.GenerateLODs(gameObject,levels,true, SimplificationOptions.Default);*/


        
        
    }

    // Update is called once per frame
    void Update()
    {
        var meshSimplifier = new MeshSimplifier(SourceMesh);
        meshSimplifier.SimplifyMesh(Quality);
        var destMesh = meshSimplifier.ToMesh();
        Skin.GetComponent<SkinnedMeshRenderer>().sharedMesh = destMesh;

    }
}
